package com.pawthunder.projecttemplate

/**
 * Constants for association with external services like admob or firebase
 * @author Peter Grajko
 */
object AppConfig {

    private const val HOST_DOMAIN = "api.themoviedb.org"

    private const val HOST_VERSION = "3"

    const val HOST= "https://$HOST_DOMAIN/$HOST_VERSION/"
}