package com.pawthunder.projecttemplate.api

import com.pawthunder.projecttemplate.vo.db.Movie
import com.pawthunder.projecttemplate.vo.reponses.MoviesListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/changes")
    fun getMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): Call<MoviesListResponse>

    @GET("/movie/{movie_id}")
    fun getMovieDetail(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String = "en-US"
    ): Call<Movie>
}