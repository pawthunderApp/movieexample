package com.pawthunder.projecttemplate.db


import androidx.room.Database
import androidx.room.RoomDatabase
import com.pawthunder.projecttemplate.vo.ExampleEntity

/**
 * Main database description.
 */
@Database(
    entities = [ExampleEntity::class],
    version = 1,
    exportSchema = false
)
abstract class MovieDb : RoomDatabase() {

}
