package com.pawthunder.projecttemplate.di

import android.app.Application
import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.pawthunder.projecttemplate.AppConfig
import com.pawthunder.projecttemplate.api.ApiService
import com.pawthunder.projecttemplate.db.MovieDb
import com.pawthunder.projecttemplate.util.LiveDataCallAdapterFactory
import com.pawthunder.projecttemplate.util.PropertyProvider
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun providOkhttpService() =
        OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()

    @Singleton
    @Provides
    fun provideGithubService(okHttpClient: OkHttpClient) =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(AppConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideDb(app: Application) =
        Room.databaseBuilder(app, MovieDb::class.java, "movies.db")
            .addMigrations()
            .fallbackToDestructiveMigration()
            .build()

}