package com.pawthunder.projecttemplate.di

import com.pawthunder.projecttemplate.ui.movie.MovieFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMovieFragment(): MovieFragment
}