package com.pawthunder.projecttemplate.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
