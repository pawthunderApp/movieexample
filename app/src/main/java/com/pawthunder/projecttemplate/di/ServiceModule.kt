package com.pawthunder.projecttemplate.di

import dagger.Module

@Suppress("unused")
@Module
abstract class ServiceModule {
}