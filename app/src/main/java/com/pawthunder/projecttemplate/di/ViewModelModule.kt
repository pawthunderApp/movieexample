package com.pawthunder.projecttemplate.di

import androidx.lifecycle.ViewModelProvider
import com.pawthunder.projecttemplate.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    /*@Binds
    @IntoMap
    @ViewModelKey(PortfolioViewModel::class)
    abstract fun bindPortfolioViewModel(portfolioModel: PortfolioViewModel): ViewModel*/

}
