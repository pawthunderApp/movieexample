package com.pawthunder.projecttemplate.repository

import android.app.Application
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import com.pawthunder.projecttemplate.api.ApiService
import com.pawthunder.projecttemplate.repository.inmemory.PageKeyDataSource
import com.pawthunder.projecttemplate.repository.movie.MovieDataSourceFactory
import com.pawthunder.projecttemplate.util.AppExecutors
import com.pawthunder.projecttemplate.util.PropertyProvider
import com.pawthunder.projecttemplate.vo.api.Listing
import com.pawthunder.projecttemplate.vo.db.Movie
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val apiService: ApiService,
    private val appExecutors: AppExecutors,
    app: Application
) {

    private val apiKey = PropertyProvider.getProperty(app.applicationContext,"apis.properties", "movieDb")

    fun loadNews(): Listing<Movie> {
        val sourceFactory = MovieDataSourceFactory(apiService, appExecutors.networkIO(), apiKey)

        val livePagedList = sourceFactory.toLiveData(
            pageSize = PageKeyDataSource.DEFAULT_PAGE_SIZE,
            fetchExecutor = appExecutors.networkIO()
        )

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }

        return Listing(
            livePagedList,
            Transformations.switchMap(sourceFactory.sourceLiveData) { it.networkState },
            refreshState,
            { sourceFactory.sourceLiveData.value?.invalidate() },
            { sourceFactory.sourceLiveData.value?.retryAllFailed() }
        )
    }
}