package com.pawthunder.projecttemplate.repository.movie

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.pawthunder.projecttemplate.api.ApiService
import com.pawthunder.projecttemplate.vo.db.Movie
import java.util.concurrent.Executor

class MovieDataSourceFactory(
    private val apiService: ApiService,
    private val networkExecutor: Executor,
    private val apiKey: String
) : DataSource.Factory<Int, Movie>() {

    val sourceLiveData = MutableLiveData<PageKeyMovieDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val source = PageKeyMovieDataSource(apiService, networkExecutor, apiKey)
        sourceLiveData.postValue(source)
        return source
    }


}