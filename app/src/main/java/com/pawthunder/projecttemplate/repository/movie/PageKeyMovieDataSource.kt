package com.pawthunder.projecttemplate.repository.movie

import androidx.paging.PageKeyedDataSource
import com.pawthunder.projecttemplate.api.ApiService
import com.pawthunder.projecttemplate.repository.inmemory.PageKeyDataSource
import com.pawthunder.projecttemplate.vo.db.Movie
import com.pawthunder.projecttemplate.vo.reponses.MoviesListResponse
import retrofit2.Response
import java.util.*
import java.util.concurrent.Executor

class PageKeyMovieDataSource(
    apiService: ApiService,
    networkExecutor: Executor,
    private val apiKey: String
) : PageKeyDataSource<Movie, MoviesListResponse>(apiService, networkExecutor) {

    override fun createInitialRequest(params: PageKeyedDataSource.LoadInitialParams<Int>) =
        apiService.getMovies(apiKey, 1)

    override fun createAfterRequest(params: PageKeyedDataSource.LoadParams<Int>) =
        apiService.getMovies(apiKey, params.key)

    override fun retrieveItems(response: Response<MoviesListResponse>) =
        response.body()?.results ?: Collections.emptyList()
}