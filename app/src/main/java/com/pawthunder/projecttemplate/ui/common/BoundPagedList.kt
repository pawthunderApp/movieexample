package com.pawthunder.projecttemplate.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.DataBindingComponent
import com.pawthunder.projecttemplate.R
import com.pawthunder.projecttemplate.util.AppExecutors
import com.pawthunder.projecttemplate.vo.api.NetworkState

abstract class BoundPagedList<T>(
    private val retryCallback: () -> Unit,
    private val bindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    diffCallback: DiffUtil.ItemCallback<T>
) : PagedListAdapter<T, RecyclerView.ViewHolder>(
    AsyncDifferConfig.Builder<T>(diffCallback)
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()
) {

    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == R.layout.network_state_item) NetworkStateItemViewHolder.create(parent, retryCallback)
        else DataBoundHolder(
            initBinding(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    viewType,
                    parent,
                    false,
                    bindingComponent
                )
            )
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NetworkStateItemViewHolder -> holder.bindTo(networkState, position)
            is DataBoundHolder<ViewDataBinding> -> {
                bind(holder.binding, getItem(position), position)
                holder.binding.executePendingBindings()
            }
        }
    }

    override fun getItemViewType(position: Int) =
        if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            getViewType(position)
        }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    abstract fun initBinding(binding: ViewDataBinding): ViewDataBinding

    abstract fun getViewType(position: Int): Int

    abstract fun bind(binding: ViewDataBinding, item: T?, position: Int)

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED
}