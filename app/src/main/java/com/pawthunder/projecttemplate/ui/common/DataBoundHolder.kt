/*
 * Copyright (c) 2018 Peter Grajko. All rights reserved.
 *
 * Developed by Peter Grajko on 11/9/18 11:37 AM.
 * Last modified 11/9/18 11:36 AM.
 */

package com.pawthunder.projecttemplate.ui.common

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class DataBoundHolder<out T : ViewDataBinding> constructor(val binding: T) : RecyclerView.ViewHolder(binding.root)