package com.pawthunder.projecttemplate.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pawthunder.projecttemplate.R
import com.pawthunder.projecttemplate.vo.api.NetworkState
import com.pawthunder.projecttemplate.vo.api.Status
import kotlinx.android.synthetic.main.network_state_item.view.*

class NetworkStateItemViewHolder(
    view: View,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(view) {

    private val progressBar = view.networkProgressBar
    private val retry = view.networkRetryButton
    private val errorMsg = view.networkErrorMessage

    init {
        retry.setOnClickListener { retryCallback() }
    }

    fun bindTo(networkState: NetworkState?, position: Int) {
        progressBar.visibility = if (networkState?.status == Status.LOADING && position > 0) View.VISIBLE else View.GONE
        retry.visibility = if (networkState?.status == Status.ERROR) View.VISIBLE else View.GONE
        errorMsg.visibility = if (networkState?.message != null) View.VISIBLE else View.GONE
        errorMsg.text = networkState?.message
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): NetworkStateItemViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.network_state_item, parent, false)
            return NetworkStateItemViewHolder(view, retryCallback)
        }
    }
}