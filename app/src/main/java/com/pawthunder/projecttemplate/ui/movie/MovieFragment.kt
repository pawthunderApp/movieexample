package com.pawthunder.projecttemplate.ui.movie

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider

import com.pawthunder.projecttemplate.R
import com.pawthunder.projecttemplate.di.Injectable
import com.pawthunder.projecttemplate.util.AppExecutors
import com.pawthunder.projecttemplate.util.autoCleared
import javax.inject.Inject

class MovieFragment : Fragment(), Injectable {

    /*@Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors*/

    /*private var binding by autoCleared<FragmentMovieBinding>()*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }
}
