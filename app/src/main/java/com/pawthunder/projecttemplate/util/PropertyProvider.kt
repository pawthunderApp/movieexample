package com.pawthunder.projecttemplate.util

import android.content.Context
import timber.log.Timber
import java.io.IOException
import java.util.*

object PropertyProvider {

    fun getProperty(context: Context, store: String, name: String): String {
        try {
            val properties = Properties()
            val assetManager = context.assets
            val inputStream = assetManager.open(store)
            properties.load(inputStream)
            return properties.getProperty(name)
        } catch(ex: IOException) {
            Timber.i("There is no such property store")
        }
        return ""
    }
}