package com.pawthunder.projecttemplate.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ExampleEntity(
    @PrimaryKey val id: Int
)