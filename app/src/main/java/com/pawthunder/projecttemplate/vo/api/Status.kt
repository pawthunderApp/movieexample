package com.pawthunder.projecttemplate.vo.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}