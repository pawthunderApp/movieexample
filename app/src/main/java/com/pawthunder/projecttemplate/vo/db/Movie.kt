package com.pawthunder.projecttemplate.vo.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.pawthunder.projecttemplate.vo.reponses.Collection
import com.pawthunder.projecttemplate.vo.reponses.Genre
import com.pawthunder.projecttemplate.vo.reponses.ProductionCountry
import com.pawthunder.projecttemplate.vo.reponses.Language
import java.math.BigDecimal

@Entity
data class Movie(
    @PrimaryKey
    val id: Int,
    var adult: Boolean?,
    @SerializedName("backdrop_path")
    var backdropPath: String?,
    @SerializedName("belongs_to_collection")
    var belongsToCollection: Collection?,
    var budget: Int?,
    var genres: List<Genre>,
    var homepage: String?,
    @SerializedName("imdb_id")
    var imdbId: String?,
    @SerializedName("original_language")
    var originalLanguage: String?,
    @SerializedName("original_title")
    var originalTitle: String?,
    var overview: String?,
    var popularity: Double?,
    @SerializedName("poster_path")
    var posterPath: String?,
    @SerializedName("production_countries")
    var productionCountries: List<ProductionCountry>,
    @SerializedName("release_date")
    var releaseDate: String?,
    var revenue: BigDecimal?,
    var runtime: String?,
    @SerializedName("spoken_languages")
    var languages: List<Language>,
    var status: String?,
    var tagline: String?,
    var title: String?,
    var video: Boolean,
    @SerializedName("vote_average")
    var voteAverage: Double?,
    @SerializedName("vote_count")
    var voteCount: Int?
)