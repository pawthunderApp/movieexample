package com.pawthunder.projecttemplate.vo.reponses

import com.google.gson.annotations.SerializedName

data class Collection(
    val id: Int,
    var name: String?,
    @SerializedName("poster_path")
    var posterPath: String?,
    @SerializedName("backdrop_path")
    var backdropPath: String?
)