package com.pawthunder.projecttemplate.vo.reponses

data class Genre(
    val id: Int,
    val name: String
)