package com.pawthunder.projecttemplate.vo.reponses

import com.google.gson.annotations.SerializedName

class Language(
    @SerializedName("iso_639_1")
    var iso: String?,
    var name: String?
)