package com.pawthunder.projecttemplate.vo.reponses

import com.google.gson.annotations.SerializedName
import com.pawthunder.projecttemplate.vo.db.Movie

data class MoviesListResponse(
    val page: Int?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("total_results")
    val totalResults: Int?,
    val results: List<Movie>
)