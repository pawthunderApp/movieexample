package com.pawthunder.projecttemplate.vo.reponses

import com.google.gson.annotations.SerializedName

data class ProductionCountry(
    @SerializedName("iso_3166_1")
    var iso: String?,
    var name: String?
)